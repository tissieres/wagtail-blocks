# wagtail-blocks

Wagtail blocks for ctrl-tissieres

## Features

- `wagtail_blocks.blocks.HeadingBlock`
- `wagtail_blocks.blocks.DetailsBlock`
- `wagtail_blocks.blocks.GalleryBlock`
- `wagtail_blocks.blocks.DocumentsListBlock`
