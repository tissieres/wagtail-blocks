from wagtail import blocks
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.images.blocks import ImageChooserBlock


class HeadingBlock(blocks.CharBlock):
    class Meta:
        template = "blocks/heading_block.html"
        icon = "title"
        label = "Titre"


class DetailsBlock(blocks.StructBlock):
    title = blocks.CharBlock(
        max_length=255,
        label="Titre",
    )
    details = blocks.RichTextBlock(
        label="Détails", features=["bold", "italic", "link", "ol", "ul"]
    )

    class Meta:
        icon = "folder-open-1"
        template = "blocks/detail_block.html"
        label = "Section repliable"


class GalleryBlock(blocks.StructBlock):
    images = blocks.ListBlock(ImageChooserBlock(), label="Galerie")

    class Meta:
        icon = "image"
        template = "blocks/gallery_block.html"
        label = "Galerie de photos"


class DocumentsListBlock(blocks.StructBlock):
    documents = blocks.ListBlock(DocumentChooserBlock(), label="Documents")

    class Meta:
        icon = "document"
        template = "blocks/documents_list_block.html"
        label = "Liste de documents"
